### Цель
Реализовать сервис, который позволяет ограничивать количество допустимых запросов в секунду (ЗВС или request per second RPS) для REST сервиса в соответствии с правилами для конкретных пользователей.

### Дано
Сервис, который возвращает максимально допустимый ЗВС для конкретного пользователя по его токену. У пользователя одновременно может быть несколько используемых токенов. В контексте данной задачи этот сервис реализовывать не нужно. Достаточно иметь моковую реализацию которая возвращает допустимый ЗВС для пары пользователей по паре токенов. Эту же реализацию можно использовать и в тесте.

### Правила работы ThrottlingService
- _Если токен не предоставлен тогда пользователь считается неавторизованным._
  - ThrottlingServiceImplTest: If token not provided - threat user unauthorized and use guest RPS


- _Все неавторизованные пользователи лимитированы GuestRPS, (передается параметром при создании ThrottlingServiceImpl, может быть равен для примера 20 или любое другое целое число)._
  - ThrottlingServiceImplTest: If token not provided - threat user unauthorized and use guest RPS


- _Если токен пришел в запросе но SlaService не вернул еще SLA по пользователю то использовать GuestRPS._
  - ThrottlingServiceImplTest: If token provided, but SLA not yet received - use guest RPS


- _ЗВС должен расчитываться по пользователю т.к. один и тот же пользователь может иметь разные токены._
  - ThrottlingServiceImplTest: if different tokens used - limit RPS by user
  

- _SLA меняется довольно редко, а SlaService довольно долгий при вызове (~200 - 300 миллисекунд на запрос). Поэтому предусмотрите кеширование SLA, и не выполняйте запрос к SlaService если запрос по такому токену уже в процессе._
  - ThrottlingServiceImplTest: if SLA cached - use it's RPS and don't request again
  - ThrottlingServiceImplTest: Do not request SLA if already in progress


- _Учтите что среднее время отклика REST сервиса должно быть не более 5 миллисекунд. т.е. вызовы ThrottlingService не должны влиять на допустимую скорость работы REST сервиса._
  - Based on 500 runs from postman/Throttling.postman_test_run.json (MacBook Pro 2019, Core i9, 32GB RAM)
    - Average without throttling - 2ms 
    - Average with throttling - 2ms 

### Критерии приема задания:
- Реализовать class ThrottlingServiceImpl implements ThrottlingService
  - Done. Decided to choose concurrent collections instead of guava cache to get the best performance. 
- Покрыть реализацию тестами которые доказывают правильность его работы.
  - Done. Decided to use Spock testing framework as more readable and functional alternative to JUnit.

### Дополнительно (по желанию и наличию времени):
- реализовать любой REST сервис использующий данную реализацию ThrottlingService.
  - FortuneCookie API (https://en.wikipedia.org/wiki/Fortune_cookie);
  - decided to implement as annotation-based interceptor.
- написать тест на нагрузочное тестирование данного REST сервиса
  - have no time for Gatling, decided to make local perf tests with Postman.
- сравнить скорость работы REST сервиса с использованием ThrottlingService и без.
    - Without slowing down.
    - Based on 500 runs from postman/Throttling.postman_test_run.json (MacBook Pro 2019, Core i9, 32GB RAM)
        - Average without throttling - 2ms
        - Average with throttling - 2ms
    
### Recommendations for further steps:
- Create cache warm-up based on latest tokens.
- Scheduled cleaning of old SLA cache items. 
- Limit SLA cache size. Create Actuator monitors.  
- Replace SlaServiceDummyImpl with dedicated SLA service and add BDD tests with Wiremock.