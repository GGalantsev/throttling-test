package com.gmail.ggalantsev.throttling

import com.gmail.ggalantsev.throttling.service.SlaService
import com.gmail.ggalantsev.throttling.service.ThrottlingService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class LoadContextTest extends Specification {

    @Autowired(required = false)
    private ThrottlingService throttlingService
    @Autowired(required = false)
    private SlaService slaService

    def "when context is loaded then all expected beans are created"() {
        expect: "the ThrottlingService is created"
            throttlingService
            slaService
    }
}
