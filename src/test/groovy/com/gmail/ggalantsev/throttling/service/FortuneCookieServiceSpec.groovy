package com.gmail.ggalantsev.throttling.service


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class FortuneCookieServiceSpec extends Specification {

    @Autowired
    FortuneCookieService fortuneCookieService

    def "it's possible to get random fortune cookie"() {
        when:
            def cookie = fortuneCookieService.getFortuneCookie()
        then:
            cookie
            cookie.id
            cookie.message
    }

    def "it's possible to get fortune cookie by id"() {
        when:
            def cookie = fortuneCookieService.getFortuneCookie(5)
        then:
            cookie
            cookie.id
            cookie.message
    }

    def "IllegalArgumentException thrown if wrong id provided"() {
        when:
            fortuneCookieService.getFortuneCookie(15)
        then:
            def e = thrown(IllegalArgumentException)
            e.message == "Fortune Cookie not found by id " + 15
    }

}
