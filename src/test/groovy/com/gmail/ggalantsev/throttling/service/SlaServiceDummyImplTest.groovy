package com.gmail.ggalantsev.throttling.service

import spock.lang.Shared
import spock.lang.Specification

import java.util.concurrent.ExecutionException

class SlaServiceDummyImplTest extends Specification {

    private static final String TOKEN = "1"
    private static final String USER = "John"
    private static final int RPS = 20

    @Shared
    SlaServiceDummyImpl slaServiceDummy = new SlaServiceDummyImpl()

    void setupSpec() {
        slaServiceDummy.generateTokens([(TOKEN): new SlaService.SLA(USER, RPS)])
    }

    def "it's possible to get SLA by token"() {
        when:
            def slaFuture = slaServiceDummy.getSlaByToken(TOKEN)
        then:
            slaFuture
            def sla = slaFuture.get()
            sla
            sla.user == USER
            sla.rps == RPS
    }

    def "if sla for token not exists SlaNotFoundException thrown"() {
        given:
            def failedToken = "non-existent"
        when:
            def slaFuture = slaServiceDummy.getSlaByToken(failedToken)
            slaFuture.get()
        then:
            def e = thrown(ExecutionException)
            e.cause.class == SlaServiceDummyImpl.SlaNotFoundException
    }

}
