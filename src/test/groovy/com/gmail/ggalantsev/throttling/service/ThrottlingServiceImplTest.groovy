package com.gmail.ggalantsev.throttling.service

import spock.lang.Specification

import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

class ThrottlingServiceImplTest extends Specification {

    public static final int GUEST_REQUEST_LIMIT = 10
    public static final String JOHNS_TOKEN = "1"
    public static final String JOHNS_ANOTHER_TOKEN = "2"
    public static final int JOHNS_REQUEST_LIMIT = 20
    public static final CompletableFuture<SlaService.SLA> SUCCESSFUL_SLA_FUTURE = CompletableFuture.completedFuture(new SlaService.SLA("John", JOHNS_REQUEST_LIMIT))

    def slaService = Mock(SlaServiceDummyImpl)
    def throttlingService = new ThrottlingServiceImpl(slaService, GUEST_REQUEST_LIMIT)

    def "If token not provided - threat user unauthorized and use guest RPS"() {
        given:
            Optional<String> token = Optional.empty()

        when:
            def allowedRequests = (0..GUEST_REQUEST_LIMIT + 10)
                    .collect { throttlingService.isRequestAllowed(token) }
                    .findAll { it }

        then:
            allowedRequests.size() == GUEST_REQUEST_LIMIT
    }


    def "If token provided, but SLA not yet received - use guest RPS"() {
        given:
            Optional<String> token = Optional.of(JOHNS_TOKEN)
        and: "SLA service response takes some time"
            slaService.getSlaByToken(JOHNS_TOKEN) >> {
                TimeUnit.SECONDS.sleep(1)
                return SUCCESSFUL_SLA_FUTURE
            }

        when:
            def allowedRequests = (0..GUEST_REQUEST_LIMIT + 10)
                    .collect { throttlingService.isRequestAllowed(token) }
                    .findAll { it }

        then:
            allowedRequests.size() == GUEST_REQUEST_LIMIT
    }

    def "if SLA cached - use it's RPS and don't request again"() {
        setup:
            _ * slaService.getSlaByToken(JOHNS_TOKEN) >> SUCCESSFUL_SLA_FUTURE
        and: "cache SLA"
            throttlingService.warmUpSlaCache(Set.of(JOHNS_TOKEN))

        when:
            def allowedRequests = (0..JOHNS_REQUEST_LIMIT + 10)
                    .collect { throttlingService.isRequestAllowed(Optional.of(JOHNS_TOKEN)) }
                    .findAll { it }

        then:
            allowedRequests.size() == JOHNS_REQUEST_LIMIT
        and:
            0 * slaService.getSlaByToken(JOHNS_TOKEN) >> SUCCESSFUL_SLA_FUTURE
    }

    def "if different tokens used - limit RPS by user"() {
        setup:
            _ * slaService.getSlaByToken(_) >> SUCCESSFUL_SLA_FUTURE
        and: "cache SLA"
            throttlingService.warmUpSlaCache(Set.of(JOHNS_TOKEN, JOHNS_ANOTHER_TOKEN))

        when:
            def allowedRequests = (0..JOHNS_REQUEST_LIMIT)
                    .collect {
                        [JOHNS_TOKEN, JOHNS_ANOTHER_TOKEN]
                                .collect { throttlingService.isRequestAllowed(Optional.of(it)) }
                    }
                    .flatten()
                    .findAll { it }

        then:
            allowedRequests.size() == JOHNS_REQUEST_LIMIT
    }

    def "Do not request SLA if already in progress"() {
        when:
            (0..50).forEach { throttlingService.isRequestAllowed(Optional.of(JOHNS_TOKEN)) }

        then:
            1 * slaService.getSlaByToken(JOHNS_TOKEN) >> {
                TimeUnit.MILLISECONDS.sleep(100)
                return SUCCESSFUL_SLA_FUTURE
            }
    }
}
