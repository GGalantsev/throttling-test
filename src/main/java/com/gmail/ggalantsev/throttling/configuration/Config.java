package com.gmail.ggalantsev.throttling.configuration;

import com.gmail.ggalantsev.throttling.controller.interceptor.RequestFrequencyCheckInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class Config implements WebMvcConfigurer {

    private RequestFrequencyCheckInterceptor requestFrequencyCheckInterceptor;

    @Autowired
    public Config(RequestFrequencyCheckInterceptor requestFrequencyCheckInterceptor) {
        this.requestFrequencyCheckInterceptor = requestFrequencyCheckInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(requestFrequencyCheckInterceptor);
    }
}
