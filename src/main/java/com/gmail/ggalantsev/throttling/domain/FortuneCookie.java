package com.gmail.ggalantsev.throttling.domain;

import lombok.Value;

@Value
public class FortuneCookie {
    Integer id;
    String message;
}
