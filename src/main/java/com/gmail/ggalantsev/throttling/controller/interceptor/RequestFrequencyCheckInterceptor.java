package com.gmail.ggalantsev.throttling.controller.interceptor;

import com.gmail.ggalantsev.throttling.service.ThrottlingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

import static java.util.function.Predicate.not;
import static org.springframework.http.HttpStatus.TOO_MANY_REQUESTS;

@Component
public class RequestFrequencyCheckInterceptor implements HandlerInterceptor {

    private final ThrottlingService throttlingService;

    @Autowired
    public RequestFrequencyCheckInterceptor(ThrottlingService throttlingService) {
        this.throttlingService = throttlingService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        boolean isAnnotated = ((HandlerMethod) handler).getMethod().getAnnotation(RequiredRequestFrequencyCheck.class) != null;
        if (!isAnnotated) {
            return true;
        }

        String token = request.getHeader("token");
        if (throttlingService.isRequestAllowed(Optional.ofNullable(token).filter(not(String::isBlank)))) {
            return true;
        }

        response.sendError(TOO_MANY_REQUESTS.value(), TOO_MANY_REQUESTS.getReasonPhrase());
        return false;
    }
}