package com.gmail.ggalantsev.throttling.controller;

import com.gmail.ggalantsev.throttling.controller.interceptor.RequiredRequestFrequencyCheck;
import com.gmail.ggalantsev.throttling.domain.FortuneCookie;
import com.gmail.ggalantsev.throttling.service.FortuneCookieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@ResponseBody
@RequestMapping(FortuneCookieController.API_PATH)
public class FortuneCookieController {

    public static final String API_PATH = "/";

    private final FortuneCookieService fortuneCookieService;

    @Autowired
    public FortuneCookieController(FortuneCookieService fortuneCookieService) {
        this.fortuneCookieService = fortuneCookieService;
    }

    @GetMapping("fortune-cookie/")
    FortuneCookie getFortuneCookie() {
        return fortuneCookieService.getFortuneCookie();
    }

    @GetMapping("fortune-cookie/{id}/")
    FortuneCookie getFortuneCookie(@PathVariable Integer id) {
        return fortuneCookieService.getFortuneCookie(id);
    }

    @RequiredRequestFrequencyCheck
    @GetMapping("throttling/fortune-cookie/")
    FortuneCookie getFortuneCookieThrottling() {
        return fortuneCookieService.getFortuneCookie();
    }

    @RequiredRequestFrequencyCheck
    @GetMapping("throttling/fortune-cookie/{id}/")
    FortuneCookie getFortuneCookieThrottling(@PathVariable Integer id) {
        return fortuneCookieService.getFortuneCookie(id);
    }

}