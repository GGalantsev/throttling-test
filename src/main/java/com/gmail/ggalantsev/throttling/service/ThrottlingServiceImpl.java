package com.gmail.ggalantsev.throttling.service;

import com.gmail.ggalantsev.throttling.service.SlaService.SLA;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

@Slf4j
@Service
public class ThrottlingServiceImpl implements ThrottlingService {

    private final SlaService slaService;

    private final ConcurrentHashMap<String, ThrottlingManager> userToThrottlingManager = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, String> tokenToUser = new ConcurrentHashMap<>();
    private final ThrottlingManager guestThrottlingManager;

    private final Set<String> slaRequestsInProgress = Collections.newSetFromMap(new ConcurrentHashMap<>());

    public ThrottlingServiceImpl(@Autowired SlaService slaService,
                                 @Value("${throttling.sla.guest-requests-limit:20}") int guestRequestsLimit) {
        this.slaService = slaService;
        this.guestThrottlingManager = new ThrottlingManager(guestRequestsLimit);
    }

    /**
     * Caches SLA for given {@param tokens}
     */
    public void warmUpSlaCache(Set<String> tokens) {
        tokens.forEach(this::addSlaToCache);
    }

    @Override
    public boolean isRequestAllowed(Optional<String> token) {
        if (token.isPresent()) {
            Optional<ThrottlingManager> throttlingManager = token
                    .map(tokenToUser::get)
                    .map(userToThrottlingManager::get);
            if (throttlingManager.isPresent()) {
                return throttlingManager.get().isRequestAllowed();
            }
            new Thread(() -> addSlaToCache(token.get())).start();
        }
        return guestThrottlingManager.isRequestAllowed();
    }

    private void addSlaToCache(String token) {
        if (!slaRequestsInProgress.add(token)) {
            return;
        }
        try {
            log.info("Requesting SLA for token '{}'", token);
            SLA sla = slaService.getSlaByToken(token).get();
            log.info("Received SLA for token '{}': {}", token, sla);
            tokenToUser.put(token, sla.getUser());
            userToThrottlingManager.put(sla.getUser(), new ThrottlingManager(sla.getRps()));
        } catch (RuntimeException e) {
            log.warn("Failed to get SLA for token '{}'", token, e);
        } catch (InterruptedException | ExecutionException e) {
            log.error("Failed to get SLA for token '{}'", token, e);
        } finally {
            slaRequestsInProgress.remove(token);
        }
    }

    /**
     * Holds latest requests timestamps and checks if new request satisfies allowed count in allowed time interval.
     */
    @ToString
    private static class ThrottlingManager {

        private static final int TIME_INTERVAL_MILLIS = 1000;

        private final int requestsLimit;
        private final LinkedList<Long> timestamps = new LinkedList<>();

        public ThrottlingManager(int requestsLimit) {
            this.requestsLimit = requestsLimit;
        }

        /**
         * Checks if oldest successful request within {@link #requestsLimit} happened earlier that {@link #TIME_INTERVAL_MILLIS} from current request.
         *
         * @return {@code true} if {@link #timestamps} count less than {@link #requestsLimit}.<br/>
         *         {@code true} if oldest request (from latest limited by requestsLimit) happened before current time subtracted by {@link #TIME_INTERVAL_MILLIS}.<br/>
         *         {@code false} if oldest request (from latest limited by requestsLimit) happened after current time subtracted by {@link #TIME_INTERVAL_MILLIS}.
         */
        public boolean isRequestAllowed() {
            long current = System.currentTimeMillis();

            synchronized (timestamps) {
                if (timestamps.size() < requestsLimit) {
                    timestamps.addLast(current);
                    return true;
                }
                Long oldestWithinLimit = timestamps.getFirst();
                if (current - oldestWithinLimit > TIME_INTERVAL_MILLIS) {
                    timestamps.addLast(current);
                    timestamps.removeFirst();
                    return true;
                }
                return false;
            }
        }
    }

}