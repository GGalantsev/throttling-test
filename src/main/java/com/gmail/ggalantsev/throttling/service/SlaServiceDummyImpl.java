package com.gmail.ggalantsev.throttling.service;

import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

@Service
public class SlaServiceDummyImpl implements SlaService {

    private final HashMap<String, SLA> tokenToSla = new HashMap<>();

    @PostConstruct
    private void init() {
        generateTokens(Map.of(
                "1", new SLA("John", 20),
                "2", new SLA("John", 20),
                "3", new SLA("Daisy", 25),
                "4", new SLA("Daisy", 25)
        ));
    }

    public void generateTokens(Map<String, SLA> tokenToSla) {
        this.tokenToSla.putAll(tokenToSla);
    }

    @Override
    public CompletableFuture<SLA> getSlaByToken(String token) {
        dummyWait();
        return Optional
                .ofNullable(tokenToSla.get(token))
                .map(CompletableFuture::completedFuture)
                .orElse(CompletableFuture.failedFuture(new SlaNotFoundException(token)));
    }

    @SneakyThrows
    private void dummyWait() {
        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(100) + 200L);
    }

    public static class SlaNotFoundException extends RuntimeException {
        public SlaNotFoundException(String token) {
            super("SLA not found for token: " + token);
        }
    }
}
