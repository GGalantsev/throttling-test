package com.gmail.ggalantsev.throttling.service;

import com.gmail.ggalantsev.throttling.domain.FortuneCookie;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class FortuneCookieService {

    private Map<Integer, FortuneCookie> fortuneCookieMap;

    @PostConstruct
    private void init() {
        InputStream predictions = Objects.requireNonNull(
                getClass().getClassLoader().getResourceAsStream("fortuneCookie/predictions.csv"),
                "can't load predictions");

        fortuneCookieMap = new BufferedReader(new InputStreamReader(predictions, StandardCharsets.UTF_8)).lines()
                .skip(1) //header
                .map(csvRecord -> csvRecord.split(";"))
                .map(idAndMessage -> new FortuneCookie(Integer.parseInt(idAndMessage[0]), idAndMessage[1]))
                .collect(Collectors.toMap(FortuneCookie::getId, Function.identity()));
    }

    public FortuneCookie getFortuneCookie() {
        int randomId = new Random().nextInt(fortuneCookieMap.size()) + 1;
        return getFortuneCookie(randomId);
    }

    public FortuneCookie getFortuneCookie(Integer id) {
        return Objects.requireNonNull(fortuneCookieMap.get(id), () -> {
            throw new IllegalArgumentException("Fortune Cookie not found by id " + id);
        });
    }
}
