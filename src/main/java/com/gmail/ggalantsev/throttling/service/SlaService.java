package com.gmail.ggalantsev.throttling.service;

import lombok.Value;

import java.util.concurrent.CompletableFuture;

public interface SlaService {

    CompletableFuture<SLA> getSlaByToken(String token);

    @Value
    class SLA {
        String user;
        int rps;
    }

}